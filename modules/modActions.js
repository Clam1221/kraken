const {MessageEmbed} = require("discord.js");

class ModActions {
  constructor() {
    this.colors = {
      red: 0xff0000,
      orange: 0xff9d00,
      yellow: 0xfff200,
      green: 0x32ff00,
      blue: 0x0c00ff,
      indigo: 0x9800ff,
      violet: 0xfa00ff
    };
  }

  mute(offender, moderator, time) {
    let embed = new MessageEmbed()
      .setTitle(`**${offender.user.tag}** has been muted`)
      .setDescription(`**Moderator**: <@${moderator.user.id}>\n**Time**: ${time}`)
      .setColor(this.colors.red);
    return embed;
  }

  unmute(offender, moderator) {
    let embed = new MessageEmbed()
      .setTitle(`**${offender.user.tag}** has been unmuted`)
      .setDescription(`**Moderator**: <@${moderator.user.id}>`)
      .setColor(this.colors.green);
    return embed;
  }

  warn(offender, moderator, reason) {
    let embed = new MessageEmbed()
      .setTitle(`**${offender.user.tag}** has been warned.`)
      .setDescription(`**Moderator**: <@${moderator.user.id}>\n**Reason**: \`${reason}\``)
      .setColor(this.colors.orange);
    return embed;
  }

  kick(offender, moderator, reason) {
    let embed = new MessageEmbed()
      .setTitle(`**${offender.user.tag}** has been kicked.`)
      .setDescription(`**Moderator**: <@${moderator.user.id}>\n**Reason**: \`${reason}\``)
      .setColor(this.colors.orange);
    return embed;
  }

  ban(offender, moderator, reason) {
    let embed = new MessageEmbed()
      .setTitle(`**${offender.user.tag}** has been banned.`)
      .setDescription(`**Moderator**: <@${moderator.user.id}>\n**Reason**: \`${reason}\``)
      .setColor(this.colors.red);
    return embed;
  }
}

module.exports = ModActions;