const Command = require("./../base/command.js");
class Unmute extends Command {
  constructor(client) {
    super(client, {
      name: "unmute",
      description: "unmute a member in your server.",
      usage: "Moderation",
      guildOnly: true,
      aliases: ["unm"],
      permLevel: "Moderator",
      category: "Moderation"
    });
  }

  run(message) {
    let member = message.mentions.members.first();
    if(!member) return message.channel.send("You must mention a user.");
    if(member.roles.highest.position >= message.member.roles.highest.position) return message.channel.send("You may not do that, check the role list");
    if(!message.channel.permissionsFor(message.client.user).has("MANAGE_ROLES")) return message.channel.send("I don't have the manage roles permission, please fix that");

    let muterole = message.guild.roles.find(r => r.name === message.settings.muteRole);
    if(!muterole) return message.channel.send("You must set up a muted role.");
    
    if(member.roles.has(muterole.id)) {
      member.roles.remove(muterole);
    } else {
      return message.channel.send("That user is not muted.");
    }

    let embed = this.client.mod.unmute(member, message.member);

    let modLog = message.guild.channels.find(c => c.name === message.settings.modLogChannel);
    if(!modLog) return message.channel.send("Please set up a mod log.");
    
    modLog.send(embed);

    message.channel.send("Success, user unmuted!");
  }
}

module.exports = Unmute;