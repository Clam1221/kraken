const Command = require("./../base/command.js");
const ms = require("ms");
class Mute extends Command {
  constructor(client) {
    super(client, {
      name: "mute",
      description: "mute a member in your server.",
      usage: "Moderation",
      guildOnly: true,
      aliases: ["m"],
      permLevel: "Moderator",
      category: "Moderation"
    });
  }

  run(message, args) {
    let member = message.mentions.members.first();
    if(!member) return message.channel.send("You must mention a user.");
    if(member.roles.highest.position >= message.member.roles.highest.position) return message.channel.send("You may not do that, check the role list");
    if(!message.channel.permissionsFor(message.client.user).has("MANAGE_ROLES")) return message.channel.send("I don't have the manage roles permission, please fix that");

    let muterole = message.guild.roles.find(r => r.name === message.settings.muteRole);
    if(!muterole) return message.channel.send("You must set up a muted role.");

    member.roles.add(muterole);

    let time = args.slice(1).join(" ");

    let embed = this.client.mod.mute(member, message.member, time);

    let modLog = message.guild.channels.find(c => c.name === message.settings.modLogChannel);
    modLog.send(embed);

    message.channel.send("Success, user muted!");

    if(time) {
      setTimeout(() => {
        member.roles.remove(muterole);
        let embed = this.client.mod.unmute(member, message.member);
        message.channel.send(`**${member.user.tag}** has been unmuted :white_check_mark:`);
        modLog.send(embed);
      }, ms(time));
    }
  }
}

module.exports = Mute;