const Command = require("./../base/command.js");
class Kick extends Command {
  constructor(client) {
    super(client, {
      name: "kick",
      description: "kick a user from your server.",
      usage: "Moderation",
      guildOnly: true,
      aliases: ["unm"],
      permLevel: "Administrator",
      category: "Moderation"
    });
  }

  async run(message, args) {
    let member = message.mentions.members.first();
    if(!member) return message.channel.send("You must mention a user.");
    if(member.roles.highest.position >= message.member.roles.highest.position) return message.channel.send("You may not do that, check the role list");
    if(!message.channel.permissionsFor(message.client.user).has("MANAGE_ROLES")) return message.channel.send("I don't have the manage roles permission, please fix that");

    let reason = args.slice(1).join(" ") || "No reason";


    let embed = this.client.mod.kick(member, message.member, reason);

    let modLog = message.guild.channels.find(c => c.name === message.settings.modLogChannel);
    if(!modLog) return message.channel.send("Please set up a mod log.");
    
    modLog.send(embed);

    await member.kick(reason);    

    message.channel.send("Success, user kicked!");
  }
}

module.exports = Kick;