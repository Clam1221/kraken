const Command = require("./../base/command.js");
class VCmute extends Command {
  constructor(client) {
    super(client, {
      name: "vcMute",
      description: "Mute all the members in a voice channel.",
      usage: "Moderation",
      guildOnly: true,
      aliases: ["vcm"],
      permLevel: "Administrator",
      category: "Moderation"
    });
  }

  async run(message) {
    let vc = message.member.voice.channel;
    vc.members.forEach(m => {
      if(m.roles.highest.position >= message.member.roles.highest) return;
      m.setMute(true, "Automatically done");
    });

    message.channel.send("Done!");
  }
}

module.exports = VCmute;